# HyperLedger 101

# Step One: Open the Hyperledger Composer Playground
Open [Composer Playground][ohc] 

You should see the **My Business Networks** screen. The **My Business Networks** page shows you a summary of the business networks you can connect to, and the identities you can use to connect to them. Don't worry about this too much for the time being, as we're going to create our own network.

# Step Two: Creating a new business network
Next, we want to create a new business network from scratch. A business network has a couple of defining properties; **a name, and an optional description**. Choose to base a new business network on an existing template.

* Click **Deploy a new business network** under the Web Browser heading to get started.

* The new business network needs a name, let's call it **my-network**.

* Optionally, you can enter a description for your business network.

* Next we must select a business network to base ours on, because we want to build the network from scratch, **click empty-business-network**.

* Now that our network is defined, click **Deploy**

# Step Three: Connecting to the business network
Now that we've created and deployed the business network, you should see a new business network card called admin for our business network my-network in your wallet. 

To connect to our business network click **Connect now** under our business network card.

# Step Four: Adding a model file
As you can see, we're in the Define tab right now, this tab is where you create and edit the files that make up a business network definition, before deploying them and testing them using the Test tab.

As we selected an empty business network template, we need to modify the template files provided. **The first step is to update the model file**. Model files define the assets, participants, transactions, and events in our business network.

**Click the Model file to view it**.

**Delete the lines of code in the model file and replace it with this**:

**Model file**
```sh
/**
 * My commodity trading network
 */
namespace org.example.mynetwork
asset Commodity identified by tradingSymbol {
    o String tradingSymbol
    o String description
    o String mainExchange
    o Double quantity
    --> Trader owner
}
participant Trader identified by tradeId {
    o String tradeId
    o String firstName
    o String lastName
}
transaction Trade {
    --> Commodity commodity
    --> Trader newOwner
}
```

# Step Five: Adding a transaction processor script file
Now that the domain model has been defined, we can define the transaction logic for the business network. 

**Click the Add a file button**.

**Click the Script file and click Add**.

**Delete the lines of code in the script file and replace it with the following code**:

**Script file**
```sh
/**
 * Track the trade of a commodity from one trader to another
 * @param {org.example.mynetwork.Trade} trade - the trade to be processed
 * @transaction
 */
async function tradeCommodity(trade) {
    trade.commodity.owner = trade.newOwner;
    let assetRegistry = await getAssetRegistry('org.example.mynetwork.Commodity');
    await assetRegistry.update(trade.commodity);
}
```

# Step Six: Deploying the updated business network
Now that we have model, script, and access control files, we need to deploy and test our business network.

Click **Deploy changes** to upgrade the business network.

# Step Seven: Testing the business network definition
Next, we need to test our business network by creating some participants (in this case Traders), creating an asset (a Commodity), and then using our Trade transaction to change the ownership of the Commodity.

Click the **Test tab** to get started.

# Step Eight : Creating participants
The first thing we should add to our business network is two participants.

Ensure that you have the **Trader tab** selected on the left, and click **Create New Participant** in the upper right.

What you can see is the data structure of a Trader participant. We want some easily recognizable data, so delete the code that's there and paste the following:
```sh
{
  "$class": "org.example.mynetwork.Trader",
  "tradeId": "TRADER1",
  "firstName": "Amit",
  "lastName": "Yedurkar"
}
```
Click **Create New** to create the participant.

You should be able to see the new Trader participant you've created. We need another Trader to test our Trade transaction though, so create another Trader, but this time, use the following data:
```sh
{
  "$class": "org.example.mynetwork.Trader",
  "tradeId": "TRADER2",
  "firstName": "Chetan",
  "lastName": "Naik"
}
```
**Make sure that both participants exist in the Trader view before moving on!**

# Step Nine : Creating an asset
Now that we have two Trader participants, we need something for them to trade. Creating an asset is very similar to creating a participant. The Commodity we're creating will have an owner property indicating that it belongs to the Trader with the tradeId of TRADER1.

Click the **Commodity tab under Assets and click Create New Asset**.

Delete the asset data and replace it with the following:

```sh
{
  "$class": "org.example.mynetwork.Commodity",
  "tradingSymbol": "SLC",
  "description": "Script Lanes Coin",
  "mainExchange": "IndiaBulls",
  "quantity": 540.8967,
  "owner": "resource:org.example.mynetwork.Trader#TRADER1"
}
```
**After creating this asset, you should be able to see it in the Commodity tab**.

# Step Ten : Transferring the commodity between the participants
Now that we have two Traders and a Commodity to trade between them, we can test our Trade transaction.

Transactions are the basis of all change in a Hyperledger Composer business network.

To test the Trade transaction:

Click the **Submit Transaction button on the left**.

**Ensure that the transaction type is Trade**.

Replace the transaction data with the following, or just change the details:

```sh
{
  "$class": "org.example.mynetwork.Trade",
  "commodity": "resource:org.example.mynetwork.Commodity#SLC",
  "newOwner": "resource:org.example.mynetwork.Trader#TRADER2"
}
```

**Click Submit**.

Check that our asset has changed ownership from TRADER1 to TRADER2, by expanding the data section for the asset. You should see that the owner is listed as resource:org.example.mynetwork.Trader#TRADER2.

To view the full transaction history of our business network, click All Transactions on the left. Here is a list of each transaction as they were submitted. You can see that certain actions we performed using the UI, like creating the Trader participants and the Commodity asset, are recorded as transactions, even though they're not defined as transactions in our business network model. These transactions are known as 'System Transactions' and are common to all business networks, and defined in the Hyperledger Composer Runtime.


[ohc]: <https://composer-playground.mybluemix.net/>


   
